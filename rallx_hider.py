import sys
from PyQt5 import QtCore, QtWidgets, QtGui
from Ui_interface import Ui_MainWindow
from process import folderHider
from os.path import expanduser


class RallxWindow(QtWidgets.QMainWindow, Ui_MainWindow):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.setupUi(self)
        self.interfaceActions()

    def interfaceActions(self):
        self.maze.setChecked(True)
        self.maze.stateChanged.connect(self.mazeStatus)
        self.start.clicked.connect(self.hiderFolder)
        self.pbwsr.clicked.connect(lambda: self.browseFile(self.pfolder))
        self.sbwsr.clicked.connect(lambda: self.browseFile(self.sfolder))

    def test(self):
        self.statusBar().showMessage("Hello")

    def mazeStatus(self):
        if(self.maze.isChecked()):
            self.maze_ptn.setEnabled(True)
            self.maze_pswd.setEnabled(True)
        else:
            self.maze_ptn.setEnabled(False)
            self.maze_pswd.setEnabled(False)

    def browseFile(self, field):
        dir = QtWidgets.QFileDialog.getExistingDirectory(self,'Select the folder',expanduser('~'))
        field.setText(dir)

    def hiderFolder(self):
        src = self.pfolder.text()
        dest = self.sfolder.text()
        sfname = self.sfolder_name.text()
        maze = self.maze.isChecked()
        maze_ptn = self.maze_ptn.text()
        maze_pswd = self.maze_pswd.text()
        folderHider(src, dest, sfname, maze, maze_ptn, maze_pswd)



app = QtWidgets.QApplication(sys.argv)
window = RallxWindow()
window.show()
app.exec_()
