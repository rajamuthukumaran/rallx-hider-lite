import os


class folderHider():
    def __init__(self, src, dest, sfname, maze, maze_ptn, maze_pswd):
        self.src = src
        self.dest = dest
        self.sfname = sfname
        self.maze = maze
        self.maze_ptn = str(maze_ptn).replace(' ', '')
        self.maze_pswd = str(maze_pswd).replace(' ', '')
        self.hideFolder()

    def hideFolder(self):
        spath = self.dest+"/."+self.sfname
        maze_elements = self.maze_ptn.split(',')
        maze_size = len(maze_elements)
        maze_path = ('/{'+self.maze_ptn+'}')*maze_size
        maze_pswd_chars = self.maze_pswd.split(',')
        maze_pswd_path = spath+'/'

        for i in maze_pswd_chars:
            maze_pswd_path += (i+'/')


        if not os.path.isdir(self.dest) and not self.maze:
            os.mkdir(spath)
            self.copyFiles(spath)

        elif self.maze:
            os.system("mkdir -p "+spath+maze_path)
            self.copyFiles(maze_pswd_path)


    def copyFiles(self, dest_path):
        os.system("mv "+self.src+" "+dest_path)
